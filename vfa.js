/**
 * @file
 * Javascript for showing the edit buttons for forms
 */

(function ($) {
  "use strict";

  Drupal.behaviors.heatMapShow = {
    attach: function (context, settings) {
      // Check jQuery version
      var ver = $.fn.jquery.split('.');
      if (parseInt(ver[0]) > 0 && parseInt(ver[1]) > 6) {
        $('.vfa-pre').on('mouseover', function() {
            $(this).children('.vfa-link').show();
        });

        $('.vfa-pre').on('mouseout', function() {
            $(this).children('.vfa-link').hide();
        });
      } else {
         $('.vfa-pre').live('mouseover', function() {
            $(this).children('.vfa-link').show();
        });

        $('.vfa-pre').live('mouseout', function() {
            $(this).children('.vfa-link').hide();
        });       
      }
    }
  }
})(jQuery);
   